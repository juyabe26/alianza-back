package com.springboot.alianza.apirest.controllers;

import com.springboot.alianza.apirest.dto.ClienteDto;
import com.springboot.alianza.apirest.dto.GeneralResponse;
import com.springboot.alianza.apirest.dto.response.ClienteResponseDto;
import com.springboot.alianza.apirest.models.entity.Cliente;
import com.springboot.alianza.apirest.services.IClienteService;
import com.springboot.alianza.apirest.utilities.ExcelUtil;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static com.springboot.alianza.apirest.utilities.Constantes.*;

@Slf4j
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ClienteRestController {

    private final IClienteService clienteService;


    @GetMapping("/clientes")
    public GeneralResponse<ClienteResponseDto> findAllCustomer() {
        log.info(FIND_ALL);
        return clienteService.findAllCustomer();

    }

    @GetMapping("/clientes/{id}")
    public GeneralResponse<ClienteResponseDto> findCustomerById(@PathVariable Long id) {

        log.info(FIND_BY_ID, id);
        return clienteService.findCustomerById(id);

    }


    @PostMapping("/clientes")
    public GeneralResponse<ClienteDto> saveCustomer(@Valid @RequestBody Cliente cliente) {
        log.info(SAVE_CLIENT, cliente.getBusinessId());
        return clienteService.saveCustomer(cliente);
    }

    @PutMapping("/clientes/{id}")
    public GeneralResponse<ClienteDto> updateCustomer(@RequestBody Cliente cliente, @PathVariable Long id) {

        log.info(UPDATE_CLIENT, id);
        return clienteService.updateCustomer(cliente, id);

    }


    @GetMapping("/clientes/buscar")
    public GeneralResponse<ClienteResponseDto> searchCustomerBySharedKey(@RequestParam("sharedKey") String sharedKey) {

        log.info(SHARED_KEY);
        return clienteService.searchCustomerBySharedKey(sharedKey);

    }


    @GetMapping("/clientes/export")
    public void exportToCSV(HttpServletResponse response) throws IOException {

        log.info(EXPORT_CSV);

        GeneralResponse<ClienteResponseDto> clients = clienteService.findAllCustomer();
        ExcelUtil.exportToExcel(response, clients.getData().getClientes());

        log.info(EXPORT_CSV_COMPLETE);

    }

}

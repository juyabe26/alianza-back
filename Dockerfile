# Utiliza una imagen base de OpenJDK para Java 17
FROM openjdk:17-oracle
# Establece el directorio de trabajo en /app
WORKDIR /app

# Copia el archivo JAR construido por Maven al contenedor
COPY build/libs/alianza-back-0.0.1-SNAPSHOT.jar alianza-back.jar

# Expone el puerto 8080 en el contenedor
EXPOSE 8080

# Comando para ejecutar la aplicación al iniciar el contenedor
CMD ["java", "-jar", "alianza-back.jar"]

